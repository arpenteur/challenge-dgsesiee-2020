On nous donne un fichier RAW. 
Arpès s'être renseigné sur votre encyclopédie préferéz sur ce qu'est le UART, on ouvre le fichier sur Audacity.
On voit un signal en créneau. En regardant le protocole UART, on essaie de reproduire directement sur le signal audio, avec une période de 4ms pour un bit. 
On prend pas en compte les morczau qui ne respecte pas le bit de parité ni le bit de terminaison. 

Ensuite on lit la partie du message à l'envers : 
0110 1010 devient 0101 0110 
et on transforme en ASCII. 
Sur les premiers morceaux on trouve DGSESIEE{. 
On se dit que c'est gagné et on finit le signal. Attention, le protocole UART utilise 4bits ou 8, mais l'énoncé indique clairement une carte son de 8bits. 
On trouve le flag du troll : DGSESIEE{ d[-_-]b  \_(''/)_/  (^_-)   @}-;---    (*^_^*)  \o/ }
